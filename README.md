This Set/Reset circuit solves metastability: https://en.wikipedia.org/wiki/Metastability_(electronics) altogether in VHDL and https://www.falstad.com/circuit/ at simulation levels.

The key to the solution is to put deoscillator to one of the routes of the signals that could go metastable with SR Nor flip-flop.
The deoscillator splits the route in two more routes with different timings, one with zero gates and one with an additional AND gate. It combines them later on into a single signal with an AND gate.

Because the latency of the splitted routes is different, then those two signals can't arrive at the combining AND gate at the same time.
Therefore the circuit can't go metastable at the simulation level, because that fast signal changes between high and low which metastability consists of aren't able to arrive at the combining AND gate at the same time.

Easiest comparison to understand the solution is to compare this circuit to a similar gate-level representation of SR nor flip-flop and see how it goes metastable with two low inputs.
This circuit just doesn't go metastable.

Designed by Veeti Eronen from Finland