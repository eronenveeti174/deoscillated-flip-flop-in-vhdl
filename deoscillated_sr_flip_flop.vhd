library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity deoscillated_sr_flip_flop is
	port (
			s : in    std_logic := '0';
			r : in    std_logic := '0';
			q : inout std_logic := '0');
end deoscillated_sr_flip_flop;

architecture Behavioral of deoscillated_sr_flip_flop is
signal feedback : std_logic;
signal deoscillator1 : std_logic;
signal deoscillator2 : std_logic;
begin

q <= r nor feedback;
feedback <= s nor deoscillator2;
deoscillator2 <= deoscillator1 and q;
deoscillator1 <= q and q;

end Behavioral;