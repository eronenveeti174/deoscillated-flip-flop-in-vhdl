library ieee;
use ieee.std_logic_1164.all;

entity deoscillated_sr_flip_flop_testbench is
end deoscillated_sr_flip_flop_testbench;

architecture behavior of deoscillated_sr_flip_flop_testbench is
    component deoscillated_sr_flip_flop is
        port (
			s : in    std_logic := '0';
			r : in    std_logic := '0';
			q : inout std_logic := '0');
    end component;
	
    signal input  : std_logic_vector(1 downto 0);
    signal output : std_logic_vector(0 downto 0);

begin
    uut: deoscillated_sr_flip_flop port map (
        s => input(1),
        r => input(0),
        q => output(0)
    );
	
	stim_proc: process
	begin
		input <= "00"; wait for 1 ps; assert output = "0" report "00 at start fails";
		input <= "11"; wait for 1 ps; assert output = "0" report "11 at start fails";
		input <= "10"; wait for 1 ps; assert output = "1" report "10 after 11 fails";
		input <= "00"; wait for 1 ps; assert output = "1" report "00 after 10 fails";
		input <= "01"; wait for 1 ps; assert output = "0" report "01 after 00 fails";
		input <= "00"; wait for 1 ps; assert output = "0" report "00 after 01 fails";
		input <= "11"; wait for 1 ps; assert output = "0" report "11 at end fails";
		wait;
	end process;
end;